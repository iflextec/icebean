<?php
date_default_timezone_set('America/Sao_Paulo');
ini_set('display_errors',1);
ini_set('safe_mode',0);
ini_set('display_startup_erros',1);
set_include_path(get_include_path() . PATH_SEPARATOR.  __DIR__ . '/models/' );
use Io\IceBean;

spl_autoload_register(
    function($_className){
        require_once str_replace( '\\', DIRECTORY_SEPARATOR, $_className) . '.php';
    }
);
class General {
    
    public static function PrintVar($_var){
        echo '<pre>';
        print_r($_var);
        echo '</pre>';
    }
}

$ib = new IceBean('mysql:dbname=hotsite;host=localhost', 'uhotsite', 'hot9xv49');

if($ib->isConnect()){
    $ib->Select(array('menu_nome' => 'name','menu_data'=>'aud_date','code'))
               //->From(array('men'=>'tbl_menu'))
               ->From(array('men'=>'tbl_menu'))
               ->Where()->NotEquals(array("aud_date" => '2014-12-29 01:45:05'));
//               ->AndWhere(array('code'=>3))
//               ->OrWhere(array('code'=>10))
//               ->AndWhere()
//               ->Between('aud_date',array('10/11/2014','10/12/2014'));
    
    General::PrintVar($ib->GetSqlString());
    //General::PrintVar($stmt->GetBind());
    $ib->ExecuteStatement();
    General::PrintVar($ib->FetchAllAssoc());
    /*
    $ib->Insert()->Into('tbl_menu')->Values(array('name'=>'Galeria','status'=>2));
    General::PrintVar($ib->GetSqlString());
    General::PrintVar($ib->ExecuteStatement());
    echo $ib->GetLastId();
     * 
     */
    
    /*
    $ib->Update('tbl_menu')->Set(array('name'=>'Galeria'))->Where(array('status'=>3));
    General::PrintVar($ib->GetSqlString());
    $ib->ExecuteStatement();
    General::PrintVar($ib->GetRowsAffected());
     * 
     */
    
    /*
    $ib->Delete()->From('tbl_menu')->Where(array('status'=>2))->AndWhere()->GreaterEqual(array('code'=>30));
    General::PrintVar($ib->GetSqlString());
    $ib->ExecuteStatement();
    General::PrintVar($ib->GetRowsAffected());
     * 
     */
    
    $ib->Select()->From(array('men' => 'tbl_menu'))->InnerJoin(array('pag'=>'tbl_page'),'code_menu')->On('men.code=pag.code_menu');
    General::PrintVar($ib->GetSqlString());
    
    $ib->ExecuteStatement();
    General::PrintVar($ib->FetchAll());
}



//$rc = new ReflectionClass('TestClass');

?>