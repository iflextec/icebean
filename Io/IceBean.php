<?php

/**
 * Projeto desenvolvido por Leandor Araujo
 * Classe de persistência usando drive PDO como camada de conexão com SGBDs
 *
 * @copyright (c) 2015, IFLEX Tecnologia http://www.iflextecnologia.com.br
 * @author Leandro Araújo <leandro.bnk@gmail.com>
 * @package Io
 */
namespace Io;

/**
 * Meio de garantir a existência dos métodos dentro das classes.
 * Esta interface é um contrato garantindo os métodos que devem existir em toda
 * camada.
 */
interface Ib {

    /**
     * Verifica se a conexão com o banco de dados está aberta garantindo
     * a comunicação com o SGBD.
     *
     * @return Boolean Estado da conexão com o SGBD
     */
    public function isConnect();
}

/**
 * Classe principal IceBean
 *
 * @access public
 * @since 1.0 Versão alpha
 */
class IceBean implements Ib{

    /**
     * String de comandos DML e DDL
     * @var String comandos DML/DDL
     */
    private $_sql=null;

    /**
     * Flag de orientação identificando o estado da conexão com SGBD
     * @var Boolean
     */
    private $_isConnected = false;

    /**
     * Usado para organizar parâmetros na criação da instrução SQL
     * @var Array
     */
    private $_bindParam = Array();

    /**
     * Guarda o alias da tabela caso seja definido um.
     * @var String
     */
    private $_tableAlias = null;

    /**
     * Guarda objeto de conexão com SGBD
     * @var Object Connection
     */
    private $_conn = false;

    /**
     * Guarda objeto de statement do PDO
     * @var Object
     */
    private $_stmt = null;

    /**
     * Retorna a quantidade de registros afetados.
     * @var Integer
     */
    private $_rowsAffected = 0;

    /**
     * Retorna erro do processo.
     * @var Array
     */
    private $_error = null;

    /**
     * Define qual schema vai ser utilizado.
     *
     * @var String
     */
    private $_shema = '';

    public function SetSchema($_schema){
        $this->_schema = $_schema;
    }

    public function GetSchema(){
        return $this->_shema;
    }
    /**
     * Retorna o estado da conexão com o banco de dados.
     * @return Boolean TRUE|FALSE
     */
    public function isConnect(){
        return $this->_isConnected;
    }


    /**
     * Inicia a conexão com o banco de dados e abre conexão caso seja passado
     * argumentos válidos pelos parâmentros.
     *
     * @param String $_dsn String de conexão do SGBD
     * @param String $_user Usuário de conexão
     * @param String $_pass Senha do usuário
     */
    function __construct($_dsn,$_user,$_pass, $_schema=null)
    {
        try {
            if(empty($_dsn))
                throw new \Exception('Não foi definido a string de conexão.');
            if(empty($_user))
                throw new \Exception('Não foi definido o usuário do SGBD.');
            if(empty($_pass))
                throw new \Exception('Não foi definido uma senha para o usuário.');
            $this->_conn = new \PDO($_dsn, $_user, $_pass,array( \PDO::ATTR_PERSISTENT => true));
            $this->_conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            if($this->_conn->getAttribute(\PDO::ATTR_DRIVER_NAME) == 'mysql')
                $this->_conn->setAttribute(\PDO::ATTR_AUTOCOMMIT,1);
            if($this->_conn->getAttribute(\PDO::ATTR_DRIVER_NAME) == 'pgsql'){
                if(!empty($_schema)){
                    $this->_shema = $_schema;
                    $result = $this->_conn->exec("SET search_path TO {$_schema}");
                }
            }
            $this->_isConnected = true;
        } catch (\PDOException $e) {
            echo $e->getMessage();
            $this->_isConnected = false;
            //Limpa memória
            unset($e);
        }catch(\Exception $ex){
            echo $ex->getMessage();
            $this->_isConnected = false;
            //Limpa memória
            unset($ex);
        }
        //Limpa memória
        unset($_dsn,$_user,$_pass);
    }


    /**
     * Método que inicia a construção da instrução de consulta em uma tabela.
     *
     * <code>
     * //Exemplos de utilização.
     * $obj->Select()->From('tblexample');
     * $obj->Select('*')->From('tblexample');
     * $obj->Select(array('coluna1','coluna2'))->From(array('tbl'=>'tblexample'));
     * </code>
     * @param string $_column
     * @return Object \Io\IceBean
     */
    public function Select($_column=null)
    {
        try{
            $query = 'SELECT ';
            //Limpa variável dos parâmetros.
            $this->_bindParam = null;
            $this->_sql = null;
            //Verifica se foi passado alguma coluna para compor o SELECT.
            if(empty($_column))
                $_column = '{columns}.*';//Se não for passado nada é usado *
            if(is_array($_column)){
                foreach($_column as $key => $column){
                    if(!is_int($key))
                        $query .= "{tbl}.{$column} as {$key},";
                    else
                        $query .= "{tbl}.{$column},";
                }
                $query = substr($query, 0,-1);
            }else{
                $column = '';
                if(strpos($_column, ',') !== false){
                    $col = explode(',', $_column);
                    if(is_array($col) && count($col)>0){
                        foreach($col as $v)
                            $column .= "{tbl}.$v,";
                        $column = substr($column, 0, -1);
                    }
                }
                $query .= $_column;
            }
            $this->_sql = "{$query} FROM";
            //Limpa memória
            unset($query,$key,$column);
        }catch(\Exception $ex){
            $this->_sql = false;
            //Limpa memória
            unset($ex);
        }
        //Limpa memória
        unset($_column);
        return $this;
    }


    /**
     * Método que é usado de forma encadeada ou não, recebe o nome da tabela como
     * argumento e pode ser um Arrray onde é composto com a chave sendo o alias
     * e o valor como o nome da tabela.
     * Quando é pasado o nome da tabela sera substituido o trecho {columns} para que
     * a instrução não tera erro de ambiguidade.
     * Exemplo do código está no método Select()
     *
     * @access public
     * @param String|Array $_table
     * @return Object \Io\IceBean
     * @throws \Exception Caso não tenha iniciado a instrução SQL
     */
    public function From($_table)
    {
        try{
            if(empty($this->_sql))
                throw new \Exception('SQL statement empty.');
            if(empty($_table))
                throw new \Exception('Table not set.');
            $table = null;
            if(is_array($_table)){
                $this->_sql = "{$this->_sql} " . current($_table) . ' as ' . key($_table);
                $this->_tableAlias = key($_table);
                $table = $this->_tableAlias;
            }else{
                $this->_sql = "{$this->_sql} {$_table} ";
                $this->_tableAlias = $_table;
                $table = $_table;
            }
            $this->_sql = str_replace('{columns}',$table,$this->_sql);
            $this->_sql = str_replace('{tbl}',$this->_tableAlias,$this->_sql);
            //Limpa memória
            unset($table);
        } catch (\Exception $ex) {
            $this->_sql = false;
            //Limpa memória
            unset($ex);
        }
        //Limpa memória
        unset($_table);
        return $this;
    }

    /**
     * Método que contempla a composição da união do tipo (LEFT JOIN) de uma declaração de
     * seleção (SELECT)
     *
     * @access public
     * @param String|Array $_table
     * @param String|Array $_column
     * @return Object \Io\IceBean
     */
    public function LeftJoin($_table,$_column=null)
    {
        return $this->Join('LEFT JOIN',$_table,$_column);
    }

    /**
     * Método que contempla a composição da união do tipo (INNER JOIN) de uma declaração de
     * seleção (SELECT)
     *
     * @param String|Array $_table
     * @param String|Array $_column
     * @return Object \Io\IceBean
     */
    public function InnerJoin($_table,$_column=null)
    {
        return $this->Join('INNER JOIN',$_table,$_column);
    }


    /**
     * Método que verifica se existe alguma instrução maliciosa na declaração SQL
     * Caso seja uma instrução SELECT é feito uma verificação para encontrar palavras
     * chave de ações diretas nos registros e estruturas de tabelas.
     *
     * @access private
     * @return Boolean
     */
    private function SqlInjection()
    {
        $state = false;
        try{
            if(preg_match( '/(SELECT)/i', $this->_sql)){
                if(preg_match( '/( DELETE | UPDATE | TRUNCATE | DROP | CREATE | SHOW )/i', $this->_sql))
                    $state = true;
            }
        } catch (\Exception $ex) {
            $state = false;
            unset($ex);
        }
        return $state;
    }


    /**
     * Método privado que é usado pelos métodos públicos do contexto JOIN
     * Este método é responsável por compor a instrução de seleção.
     *
     * <code>
     * $ib->Select()
     *    ->From(array('alias1' => 'tblnome1'))
     *    ->InnerJoin(array('alias2'=>'tblnome2'),'code2')
     *    ->On('alias1.code=alias2.code2');
     * </code>
     *
     * @param String $_type
     * @param String|Array $_table
     * @param String|Array $_column
     * @return Object\Io\IceBean
     * @throws \Exception
     */
    private function Join($_type, $_table, $_column=null)
    {
        try{
            //Verifica se o nome da tabela esta definido.
            if(empty($_table))
                throw new \Exception('Table is not set in LEFT JOIN.');

            //Declara variáveis de escopo.
            $alias = null;
            $column = '';
            $table =null;
            //Copia o que já existe declarado de comandos SQL
            $sql = $this->_sql;
            //Encotra a declaração FROM dentro do SQL já composto.
            $pos = strripos($sql,'FROM');
            if(!empty($pos)){
                //Retorna o SQL sem a declaração FROM e concatenado uma (,) vírgula.
                //Isto é feito para que possa concatenar mais colunas da declaração JOIN que está
                //preste a iniciar sua composição na instrução de seleção SELECT
                if($_column === false)
                    $sql = substr($sql, 0,($pos) -1);
                else
                    $sql = substr($sql, 0,($pos) -1) . ', ';
            }
            //Verifica se o nome da tabela tem um Alias ou não
            if(is_array($_table)){
                $table = current($_table) . ' as ' . key($_table);
                $alias = key($_table);
            }else{
                $table = $_table;
                $alias = $_table;
            }
            //Verifica se foi passado alguma coluna específica na declaração do JOIN
            //é usado para quando não tem a necessidade de retornar todas as colunas do JOIN agregado.
            if($_column === false)
                $column = '';
            elseif(empty($_column)){
                $column = "{$alias}.*"; //Se não foi passado nenhuma coluna é retornado todas as colunas.
            }elseif(is_array($_column)){
                //Se encontrar colunas definidas é feito uma varredura para agregar o alias ou nome da tabela a qual essas colunas pertence.
                foreach($_column as $key => $value){
                    if(gettype($key) == 'string')
                        $column .= "{$alias}.{$value} AS {$key},";
                    else
                        $column .= "{$alias}.{$value},";
                }
                $column = substr($column, 0, -1);
            }elseif(is_string($_column)){
                if(strpos($_column, ',') !== false){
                    $col = explode(',', $_column);
                    if(is_array($col) && count($col)>0){
                        foreach($col as $v)
                            $column .= "{$alias}.$v,";
                        $column = substr($column, 0, -1);
                    }
                }
                $column .= $_column;

            }else//Caso não seja válidado as colunas passadar é gerado um erro.
                throw new \Exception('Error set columns LEFT JOIN.');
            //Realiza a união da instrução composta neste método com o resto da declaração inicial do SQL.
            $this->_sql = "{$sql}{$column} ".substr($this->_sql,$pos);
            //Concatena a declaração JOIN na instrução SQL.
            $this->_sql .= " {$_type} {$table}";

            //Limpa memória
            unset($alias,$table,$column,$sql,$pos);
        } catch (\Exception $ex) {
            $this->_sql = false;
            //Limpa memória
            unset($ex);
        }
        //Limpa memória
        unset($_type,$_table,$_column);
        return $this;
    }

    /**
     * Método que vai definir a cláusula WHERE de uma instrução SQL
     * Este método é responsável por já preparar os identificadores usados no PDO
     * de forma a preencher uma variável do escopo da classe que será usado posteriormente por outro método.
     * Toda a definição que este método faz é importante para que a preparação da instrução SQL
     * seja correta e válida pelas parametrizações do PDO.
     *
     * @param String|Array $_clause
     * @param String $_type
     * @param String $_operator
     * @return boolean
     * @throws \Exception
     */
    private function SetClause($_clause, $_type=null, $_operator='=')
    {
        $state = false;
        try{
            //Variáveis do escopo
            $id = 0;
            $sql = null;
            $sep = empty($_type) ? ',':null;

            if(is_array($_clause)){
                if(!empty($this->_bindParam)){
                    $id = count($this->_bindParam);
                    $id++;
                }
                $ckWhere = substr($this->_sql,strpos($this->_sql,'WHERE'));

                foreach(array('=','!','<','>','BETWEEN') as $v)
                    if(strpos($ckWhere,$v))
                        $ckWhere=true;
                if( ($id > 1 || ($id == 0 && $ckWhere === true)) && strpos($this->_sql,'WHERE') !== false && strpos($this->_sql,'UPDATE') === false)
                    $this->_sql .= " {$_type} ";

                foreach($_clause as $key => $where){
                    $this->_bindParam[":{$key}{$id}"] = $where;
                    if(!empty($this->_tableAlias))
                        $this->_sql .= " {$this->_tableAlias}.{$key} {$_operator} :{$key}{$id} {$_type} {$sep}";
                    else
                        $this->_sql .= " {$key} {$_operator} :{$key}{$id} {$_type} {$sep}";
                    $id++;
                }
                $sql = substr($this->_sql, 0, strripos($this->_sql,$_type) -1);
                $this->_sql = $sql;
            }else{
                if(!empty($_clause))
                    $this->_sql .= " {$_clause} ";
            }

            $state = true;
            //Limpa memória
            unset($id,$key,$where,$sql);
        } catch (\Exception $ex) {
            $state = false;
            //Limpa memória
            unset($ex);
        }
        //Limpa memória
        unset($_clause,$_type,$_operator);
        return $state;
    }

    /**
     * Método que define a tabela que será usado na instrução JOIN
     *
     * @param String|Array $_on
     * @return Object \Io\IceBean
     * @throws \Exception
     */
    public function On($_on)
    {
        try{
            if(empty($_on))
                throw new \Exception('Clause ON not set.');

            if(is_array($_on)){
                $this->_sql .= " ON ";
            }else{
                $this->_sql .= " ON {$_on}";
            }
        } catch (\Exception $ex) {
            $this->_sql = false;
            //Limpa memória
            unset($ex);
        }
        //Limpa memória
        unset($_on);
        return $this;
    }

    /**
     * Método que recebe a instrução definida na cláusula WHERE
     * Este método recebe um array que é definido de forma simples sempre com
     *
     * @param String|Array $_where
     * @return Object \Io\IceBean
     * @throws \Exception
     */
    public function Where($_where=null)
    {
        try{
            if(empty($_where))
                $this->_sql .= ' WHERE';
            else{
                $this->_sql .= ' WHERE';
                if($this->SetClause($_where, 'AND') === false)
                    throw new \Exception('Error set clause');
            }
        } catch (\Exception $ex) {
            $this->_sql = false;
            //Limpa memória
            unset($ex);
        }
        //Limpa memória
        unset($_where);
        return $this;
    }
    public function Limit($_limit=null)
    {

        try{
            if(empty($_limit))
                throw new \Exception('LIMIT not set.');
            else{
                if($this->_conn->getAttribute(\PDO::ATTR_DRIVER_NAME) == 'pgsql'){
                    if(is_array($_limit)){
                        $this->_sql .= " LIMIT {$_limit[1]} OFFSET {$_limit[0]} ";
                    }else
                        $this->_sql .= " LIMIT {$_limit} ";
                }else{
                    $this->_sql .= ' LIMIT '. $_limit;
                }
            }
        } catch (\Exception $ex) {
            $this->_sql = false;
            //Limpa memória
            unset($ex);
        }

        //Limpa memória
        unset($_limit);
        return $this;
    }

    public function AndWhere($_where=null)
    {
        try{
            $this->_sql .= ' AND';
            if(!empty($_where)){
                if($this->SetClause($_where, 'AND') === false)
                    throw new \Exception('Error set clause');
            }
        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }

    public function OrWhere($_where=null)
    {
        try{
            $this->_sql .= ' OR';
            if(!empty($_where)){
                if($this->SetClause($_where, 'OR') === false)
                    throw new \Exception('Error set clause');
            }
        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }

    public function NotIn($_column, $_ids=null)
    {
        try{
            $id=0;
            if(empty($_column))
                throw new \Exception('NOT IN Invalid.');
            if(!empty($this->_bindParam)){
                $id = count($this->_bindParam);
            }

            if($id > 0 && strpos($this->_sql,'WHERE') ==  true)
                $this->_sql .= " AND ";
            else
                $this->_sql .= " WHERE ";
            if(is_array($_column)){
                $col = key($_column);
                $val = $_column[$col];
                if(is_array($_ids))
                    $_ids = implode (',', $_ids);

                $this->_sql .= "{$col}.{$val} NOT IN({$_ids})";
            }else{
                if(empty($_ids))
                    $this->_sql .= " {$_column}";
                else
                    $this->_sql .= " {$_column} NOT IN({$_ids}) ";
            }
        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }

    public function In($_column, $_ids=null)
    {
        try{
            $id=0;
            if(empty($_column))
                throw new \Exception('IN Invalid.');
            if(!empty($this->_bindParam)){
                $id = count($this->_bindParam);
            }

            if(strpos($this->_sql,'WHERE') !== false){
                if($id > 0)
                    $this->_sql .= " AND ";
            }else
                $this->_sql .= " WHERE ";
            if(is_array($_column)){
                $col = key($_column);
                $val = $_column[$col];
                if(is_array($_ids))
                    $_ids = implode (',', $_ids);

                $this->_sql .= "{$col}.{$val} IN({$_ids})";
            }else{
                if(is_array($_ids))
                    $_ids = implode (',', $_ids);
                if(empty($_ids))
                    $this->_sql .= " {$_column}";
                else
                    $this->_sql .= " {$_column} IN({$_ids}) ";
            }
        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }

    public function Between($_column, $_where)
    {
        try{
            if(empty($_column))
                throw new \Exception('Column not set.');
            if(empty($_where) || !is_array($_where))
                throw new \Exception('Between not set.');
            $this->_sql .= " {$_column} BETWEEN '{$_where[0]}' AND '{$_where[1]}'";

        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }



    public function GreaterThen($_clause,$_type='AND')
    {
        try{
            if(!empty($_clause)){
                if($this->SetClause($_clause, $_type,'>') === false)
                    throw new \Exception('Error set clause');
            }

        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }
    public function LessThen($_clause,$_type='AND')
    {
        try{
            if(!empty($_clause)){
                if($this->SetClause($_clause, $_type,'<') === false)
                    throw new \Exception('Error set clause');
            }

        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }
    public function GreaterEqual($_clause,$_type='AND')
    {
        try{
            if(!empty($_clause)){
                if($this->SetClause($_clause, $_type,'>=') === false)
                    throw new \Exception('Error set clause');
            }

        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }
    public function LessEqual($_clause,$_type='AND')
    {
        try{
            if(!empty($_clause)){
                if($this->SetClause($_clause, $_type,'<=') === false)
                    throw new \Exception('Error set clause');
            }

        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }
    public function NotEquals($_clause,$_type='AND')
    {
        try{
            if(!empty($_clause)){
                if($this->SetClause($_clause, $_type,'<>') === false)
                    throw new \Exception('Error set clause');
            }

        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }

    /**
     * Método ainda em construção.
     * Estou usando a string completa sendo enviada pelo cliente.
     * @param type $_clause
     * @return \Io\IceBean
     */
    public function Like($_clause, $_type='AND')
    {
        try{
            if(!empty($_clause)){
                if(strpos($this->_sql,'WHERE'))
                    $this->_sql .= " {$_type} ";
                $this->_sql .= " {$_clause} ";
            }

        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }

    public function ExecuteStatement()
    {
        $state = false;
        try{
            if(!empty($this->_bindParam)){
                foreach($this->_bindParam as $k => $v){
                    if(strpos($v,'`') !== false){
                        $v = str_replace('`','',$v);
                        $this->_sql = str_replace($k, $v ,$this->_sql);
                        unset($this->_bindParam[$k]);
                    }
                }
            }

            $this->_stmt = $this->_conn->prepare($this->_sql);
            if(is_array($this->_bindParam)){
                foreach($this->_bindParam as $key => $value){
                    $this->_stmt->bindValue($key,$value,(gettype($value)=="string"?\PDO::PARAM_STR:\PDO::PARAM_INT));
                }
            }elseif(!empty($this->_bindParam)){
                $this->_stmt->bindValue(key($this->_bindParam),$this->_bindParam,(gettype($value)=="string"?\PDO::PARAM_STR:\PDO::PARAM_INT));
            }
            //Verifica se existe alguma instrução maliciosa no SQL que foi gerado.

            if($this->SqlInjection() === false){
                $state = $this->_stmt->execute();
                //var_dump($this->_stmt->fetchAll(\PDO::FETCH_ASSOC));
                if($state === false)
                    throw new \PDOException('Erro: Execute statement.');
            }else
                throw new \PDOException('Erro: Identified SqlInjection in statement.');
            //$state = $this->_stmt->debugDumpParams();
        } catch (\PDOException $ex) {
            $this->_error = array_merge($this->_conn->errorInfo(), array($ex->getMessage()));
            $state = false;
        }
        return $state;
    }



    public function Insert()
    {
        try{
            $this->_sql = ' INSERT INTO';
            $this->_bindParam = null;
        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }
    public function Into($_table)
    {
        $state = false;
        try{
            if(empty($this->_sql))
                throw new \Exception('Not signed statement.');
            if(is_array($_table) || empty($_table))
                throw new \Exception('Table is not set, this value does not be an array');
            $this->From($_table);
            $state = $this;
        } catch (\Exception $ex) {
            $this->_sql = false;
            $state = false;
            echo $ex->getMessage();
            unset($ex);
        }
        unset($_table);
        return $state;
    }

    public function Values($_values)
    {
        try{
            if(empty($_values))
                throw new \Exception('Column(s) is not set.');

            if(!is_array($_values))
                throw new \Exception('Value is not Array()');

            if(!empty($this->_bindParam)){
                $id = count($this->_bindParam);
                $id++;
            }else{
                $id=0;
            }

            $this->_sql .= " (";
            $strValues = ') VALUES (';
            foreach($_values as $key=>$value){
                $this->_bindParam[":{$key}{$id}"] = $value;
                $this->_sql .= "{$key},";
                $strValues .= ":{$key}{$id},";
                $id++;
            }
            $sql = substr($this->_sql, 0, -1);
            $strValues = substr($strValues, 0, -1) . ")";
            $this->_sql = "{$sql} {$strValues}";
        } catch (\Exception $ex) {
            $this->_sql=false;
        }
        return $this;
    }

    /**
     * Método para realizar vários inserts em uma tabela.
     * <code>
     * $adapter->Insert()->Into('tbl_example')->Bulk('code,path',Array(array($vars[0]),array($vars[1])))->ReturningId('code');
     * $adapter->Insert()->Into('tbl_example')->Bulk(array('code','path'),Array(array($vars[0]),array($vars[1])))->ReturningId('code');
     * </code>
     * @param String|Array $_columns
     * @param Array $_values
     * @return \Io\IceBean
     * @throws \Exception
     */
    public function Bulk($_columns, $_values)
    {
        try{
            if(empty($_values))
                throw new \Exception('Column(s) is not set.');

            if(!is_array($_values))
                throw new \Exception('Value is not Array()');

            if(!empty($this->_bindParam)){
                $id = count($this->_bindParam);
                $id++;
            }else{
                $id=0;
            }

            $this->_sql .= " (";
            if(is_array($_columns) && count($_columns) > 1){
                foreach($_columns as $column){
                    $this->_sql .= "{$column},";
                }
                $this->_sql = substr($this->_sql, 0, -1);
            }else{
                $column = is_array($_columns)?$_columns[0]:$_columns;
                $this->_sql .= " {$column} ";
            }
            $this->_sql = $this->_sql . ') ';
            $strValues = ' VALUES ';
            foreach($_values as $val){
                $strValues .= '(';
                foreach($val as $key=>$value){
                    if(isset($_columns[0])){
                        $this->_bindParam[":{$_columns[$key]}{$id}"] = $value;
                        $strValues .= ":{$_columns[$key]}{$id},";
                    }else{
                        $this->_bindParam[":{$_columns}{$id}"] = $value;
                        $strValues .= ":{$_columns}{$id},";
                    }
                    $id++;
                }
                $strValues = substr($strValues, 0, -1);
                $strValues .= '),';
            }
            $strValues = substr($strValues, 0, -1);
            $this->_sql = "{$this->_sql} {$strValues}";
        } catch (\Exception $ex) {
            $this->_sql=false;
        }
        return $this;
    }


    public function Update($_table)
    {
        try{
            if(empty($_table)||is_array($_table))
                throw new \Exception('Table is not array and not can is empty');
            $this->_sql = " UPDATE {$_table}";
            $this->_bindParam = null;
        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }

    public function Set($_where=null)
    {
        try{
            if(empty($_where))
                throw new \Exception('SQL SET values not defined');
            $this->_tableAlias = null;
            $this->_sql .= ' SET';
            if($this->SetClause($_where) === false)
                throw new \Exception('Error set clause');

        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }


    public function Delete()
    {
        try{
            $this->_sql = " DELETE FROM";
            $this->_bindParam = null;
        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }


    public function GroupBy($_group)
    {
        try{
            $this->_sql .= " GROUP BY {$_group}";
        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }

    public function OrderBy($_order)
    {
        try{
            if(is_array($_order))
                $_order = implode(',',$_order);
            $this->_sql .= " ORDER BY {$_order}";
        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }

    public function ReturningId($_column)
    {
        try{
            $this->_sql .= " RETURNING {$_column}";
        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $this;
    }

    public function NextVal($_seq)
    {
        $status = false;
        try{
            $obj = $this->_conn->query("SELECT NEXTVAL('{$_seq}') AS next_value");
            $result = $obj->fetchAll();
            if(isset($result[0]['next_value']))
                $status=$result[0]['next_value'];
            else
                $status=false;
            //var_dump($this->_conn->query("select currval('{$_seq}'::regclass)"));

        } catch (\Exception $ex) {
            $this->_sql = false;
        }
        return $status;
    }
    /**
     * Método que inicia uma transação no banco de dados para que seja possível
     * executar várias instruções SQL e validações antes de ser gravadas direto no
     * banco todas as declarações executadas.
     *
     * @return boolean
     */
    public function StartTransaction($_savepoint=null)
    {
        $state = false;
        try{
            if(!$this->_conn->getAttribute(\PDO::ATTR_DRIVER_NAME) == 'pgsql')
                $this->_conn->setAttribute(\PDO::ATTR_AUTOCOMMIT,false);
            if($this->InTransaction() === false)
                $status = $this->_conn->beginTransaction();
            else{
                $this->Exec("SAVEPOINT {$_savepoint};");
                $status=true;
            }
        } catch (\Exception $ex) {
            //echo $ex->getMessage();
            $state=false;
        }
        return $state;
    }




    public function InTransaction()
    {
        $status = false;
        try{
            $status = $this->_conn->inTransaction();
        } catch (\Exception $ex) {
            //echo $ex->getMessage();
            $status=false;
        }
        return $status;
    }

    /**
     * Método que confirma e escreve no SGBD o que foi executado durante
     * a transação aberta.
     *
     * @return Boolean
     */
    public function Commit(){
        return $this->_conn->commit();
    }

    /**
     * Método que cancela uma ou várias instruções executadas durante a transação
     * que foi iniciada.
     *
     * @return boolean
     */
    public function RollBack(){
        return $this->_conn->rollBack();
    }

    public function FetchAllAssoc()
    {
        return $this->_stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    public function FetchAll()
    {
        return $this->_stmt->fetchAll(\PDO::FETCH_OBJ);
    }
    public function GetLastId()
    {
        if($this->_conn->getAttribute(\PDO::ATTR_DRIVER_NAME) == 'pgsql'){
            $code = $this->_stmt->fetchAll(\PDO::FETCH_ASSOC);
            if(isset($code[0]))
                $code=$code[0];
            return $code;
        }else{
            return $this->_conn->lastInsertId();
        }
    }
    public function GetRowsAffected()
    {
        return $this->_stmt->rowCount();
    }
    public function GetSqlString()
    {
        $output=null;
        if(empty($this->_bindParam))
            $output = $this->_sql;
        else{
//            echo '<pre>';
//            print_r($this->_bindParam);
//            echo '</pre>';
            $query=$this->_sql;
            foreach($this->_bindParam as $key => $val){
                $query = str_replace($key, ((ctype_digit($val))?$val:"'{$val}'"), $query);
            }
            $output = $query;
        }
        return $output;
    }

    public function GetBind()
    {
        return $this->_bindParam;
    }
    public function GetError(){
        return $this->_error;
    }

    public function Exec($_query){
        return $this->_conn->exec($_query);
    }
    public function Query($_query){
        return $this->_conn->query($_query);
    }

    /**
     * Método de identificação da Classe.
     * Retorna o nome da classe.
     * @return String
     */
    public function __toString() {
        return get_class ($this );
    }
}
